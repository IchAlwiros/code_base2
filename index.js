const express = require('express')

const dotenv = require('dotenv');
dotenv.config()

const app = express()


// MIDDLEWARE
app.use(express.json())
app.use(express.urlencoded({extended:true}))


// ROUTER

const {
    Books
} = require("./src/router/index")
app.use("/books", Books)


// PORT
const port = process.env.PORT || 8000

app.listen(port, ()=>{
    console.log(`server running on port ${port}`)
})


// TEST CONNECTION DB
const {connection} = require('./src/config/config')
connection