const mysql = require('mysql2')
const dotenv = require('dotenv');
dotenv.config()

const {DB_HOST, DB_PASSWORD, DB_NAME, DB_USER } = process.env

const db = mysql.createConnection ({
    host     : DB_HOST,
    user     : DB_USER,
    password : DB_PASSWORD,
    database : DB_NAME
})

const connection = mysql.createPool({
    host     : 'localhost',
    user     : 'root',
    password : 'password',
    database : 'db_book'
}).getConnection((err) => {
    if (err) throw err;
    console.log('Connected to MySql Server')
});

module.exports = {db, connection}