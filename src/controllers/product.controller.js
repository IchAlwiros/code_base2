const {db} = require('../config/config')
const response = require('../response/responses')

const getAll = (req, res)=>{
    const sql = "SELECT * FROM book"
    db.query(sql, (error, result) => {
        response(res, 200, result, 'get all data book')
    })
}


module.exports = {
    getAll
}
