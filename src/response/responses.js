const response = (res, statusCode, data, message) =>{
    return res.status(statusCode).json({
        payload:{
            status_code: statusCode,
            message : message,
            data: data
        }
    })
}

module.exports = response